program GramShmidt
  implicit none

  real*8, allocatable :: A(:,:), Q(:, :), R(:,:), QR(:,:), D(:,:)
  integer size, randMatrix, err, i, j, k
  real*8 :: beginTime, endTime, colNorm, matrixNorm, totalTime
  real*8 :: ANorm, DNorm

  read *, size

  allocate(A(size, size))
  allocate(Q(size, size))
  allocate(R(size, size))
  allocate(QR(size, size))
  allocate(D(size, size))

  Q = 0
  R = 0

  err = randMatrix(A, size)

  call CPU_TIME(beginTime)

  do j = 1, size
    do k = 1, size
      Q(k, j) = A(k, j)
    enddo

    do i = 1, j - 1
      do k = 1, size
        R(i, j) = R(i, j) + Q(k, i) * A(k, j)
      enddo
      do k = 1, size
        Q(k, j) = Q(k, j) - R(i, j) * Q(k, i)
      enddo
    enddo

    R(j, j) = colNorm(Q, size, j)

    do k = 1, size
      Q(k, j) = Q(k, j) / R(j, j)
    enddo
  enddo

  call CPU_TIME(endTime)
  totalTime = endTime - beginTime


  QR = matmul(Q, R)

  D = A - QR

  ANorm = matrixNorm(A, size)
  DNorm = matrixNorm(D, size)

  print *, DNorm / ANorm
  print *, totalTime

  deallocate(A)
  deallocate(Q)
  deallocate(R)
  deallocate(QR)
  deallocate(D)

end program GramShmidt



integer function randMatrix(A, size)
  implicit none
  integer size
  real*8 A(size, size)
  call random_number(A)
  A = -1d0 + 2d0 * A
  randMatrix = 0
end function randMatrix

real*8 function matrixNorm(A, size)
  implicit none
  integer size, i, j
  real*8 A(size, size), result
  result = 0d0
  do i = 1, size
    do j = 1, size
      result = result + A(i,j) * A(i,j)
    enddo
  enddo
  matrixNorm = dsqrt(result)
end function matrixNorm

real*8 function colNorm(A, size, col)
  implicit none
  integer size, i, col
  real*8 A(size, size), result
  result = 0d0
  do i = 1, size
    result = result + A(i, col) * A(i, col)
  enddo
  colNorm = dsqrt(result)
end function colNorm