#include <iostream>
#include <cstdlib>
#include <ctime>
#include <cstring>
#include <cmath>
#include <cfloat>

using namespace std;

long long size;

double fRand(double fMin, double fMax)
{
    double f = (double) rand() / RAND_MAX;
    return fMin + f * (fMax - fMin);
}

void randMatrix(double* &matrix)
{
    for (int i = 0; i < size; ++i) {
        for (int j = 0; j < size; ++j) {
            matrix[size * j + i] = -1.0 + (double) rand() / RAND_MAX * 2.0;
        }
    }
}

void printMatrix(double* &matrix)
{
    for (int i = 0; i < size; ++i) {
        for (int j = 0; j < size; ++j) {
            cout.precision(6);
            cout.width(12);
            cout << matrix[size * j + i];
        }
        cout << endl;
    }
}

double colNorm(double* &matrix, long long col)
{
    double norm = 0;
    for (long long i = 0; i < size; i++) {
        norm += matrix[col * size + i] * matrix[col * size + i];
    }
    norm = sqrt(norm);
    return norm;
}

double matrixNorm(double* &matrix)
{
    double norm = 0;
    for (long long i = 0; i < size; i++) {
    	for (long long j = 0; j < size; j++) {
    		norm += matrix[size * j + i] * matrix[size * j + i];
    	}
    }
    norm = sqrt(norm);
    return norm;
}

void mulMatrix(double* &res, double* &m1, double* &m2)
{
	for (long long i = 0; i < size; i++) {
    	for (long long j = 0; j < size; j++) {
    		res[j * size + i] = 0.0;
    		for (long long k = 0; k < size; k++) {
    			res[j * size + i] += m1[k * size + i] * m2[j * size + k];
    		}
    	}
    }
}

void subMatrix(double* &res, double* &m1, double* &m2)
{
	for (long long i = 0; i < size; i++) {
    	for (long long j = 0; j < size; j++) {
    		res[j * size + i] = m1[size * j + i] - m2[size * j + i];
    	}
    }
}

int main(int argc, char const *argv[])
{
	srand(time(NULL));
    size = atol(argv[1]);
	
	double *matrix 	= new double[size * size];
	double *q 		= new double[size * size];
	double *r 		= new double[size * size];
	double *res 	= new double[size * size];
	double *dif 	= new double[size * size];
	randMatrix(matrix);

	unsigned int start_time =  clock();

	for (long long j = 0; j < size; j++) {
        for (long long k = 0; k < size; k++) {
            q[j * size + k] = matrix[j * size + k];
        }

        for (long long i = 0; i < j; i++) {
            for (long long k = 0; k < size; k++) {
                
                r[j * size + i] += q[i * size + k] * matrix[j * size + k];
            }              
            for (long long k = 0; k < size; k++) {
            	q[j * size + k] -= r[j * size + i] * q[i * size + k];
            }
            
        }
        r[j * size + j] = colNorm(q, j);
        for (long long k = 0; k < size; k++) {
            q[j * size + k] /= r[j * size + j];
        }
        while (colNorm(matrix, j) / colNorm(q, j) > 10) {
           for (long long i = 0; i < j; i++) {
            for (long long k = 0; k < size; k++) {
                    r[j * size + i] += q[i * size + k] * matrix[j * size + k];
                }              
                for (long long k = 0; k < size; k++) {
                    q[j * size + k] -= r[j * size + i] * q[i * size + k];
                }
            }
            r[j * size + j] = colNorm(q, j);
            for (long long k = 0; k < size; k++) {
                q[j * size + k] /= r[j * size + j];
            } 
        }
    }

    unsigned int end_time = clock();
    mulMatrix(res, q, r);

    const double resNorm 	= matrixNorm(res);
    const double aNorm		= matrixNorm(matrix);
    subMatrix(dif, matrix, res);
    const double difNorm	= matrixNorm(dif);

    //Print reslut matrixes
    if (argc >= 3 && strncmp(argv[2], "-p", 2) == 0) {
        cout << endl << "Q =" << endl;
	    printMatrix(q);
	    cout << endl << "R =" << endl;
	    printMatrix(r);
	    cout << endl << "A =" << endl;
	    printMatrix(matrix);
	    cout << endl << "QR =" << endl;
	    printMatrix(res);
	    cout << endl;
    }

    cout << endl << "||A|| = " << aNorm << endl;
	cout << endl << "||A - QR|| / ||A|| = " << difNorm / aNorm << endl << endl;
    cout << "TIME: " << (end_time - start_time) / (double) CLOCKS_PER_SEC << ";" << endl;

	return 0;
}