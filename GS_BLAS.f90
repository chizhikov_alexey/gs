program GramShmidtBlas
  implicit none

  real*8, allocatable :: A(:,:), Q(:, :), R(:,:), QR(:,:), D(:,:)
  integer size, randMatrix, err, i, j, k
  real*8 :: beginTime, endTime, colNorm, matrixNorm, totalTime, ddot, dnrm2
  real*8 :: ANorm, DNorm, dlange

  read *, size

  allocate(A(size, size))
  allocate(Q(size, size))
  allocate(R(size, size))
  allocate(QR(size, size))
  allocate(D(size, size))

  Q = 0
  R = 0

  err = randMatrix(A, size)

  call CPU_TIME(beginTime)

  do j = 1, size
    Q(:, j) = A(:, j)
  call dgemv('T', size, j - 1, 1d0, Q, size, A(1, j), 1, 0d0, R(1, j), 1)
    do i = 1, j - 1
      !R(i, j) = ddot(size, Q(:, i), 1, A(:, j), 1)
      call daxpy(size, -R(i, j), Q(:, i), 1, Q(:, j), 1)
    enddo
    R(j, j) = dnrm2(size, Q(:, j), 1)
    call dscal(size, 1d0 / R(j, j), Q(:, j), 1)
  enddo

  call CPU_TIME(endTime)
  totalTime = endTime - beginTime

  QR = matmul(Q, R)

  D = A - QR

  ANorm = dlange('F', size, size, A, size)
  DNorm = dlange('F', size, size, D, size)

  print *, DNorm / ANorm
  print *, totalTime

  deallocate(A)
  deallocate(Q)
  deallocate(R)
  deallocate(QR)
  deallocate(D)

end program GramShmidtBlas



integer function randMatrix(A, size)
  implicit none
  integer size
  real*8 A(size, size)
  call random_number(A)
  A = -1d0 + 2d0 * A
  randMatrix = 0
end function randMatrix
