program QR_LAPACK
  implicit none

  real*8, allocatable :: IN(:,:), A(:,:), WORK(:), TAU(:), Q(:,:), R(:,:), D(:,:), QR(:,:)
  integer size, lda, info, lwork, i, j
  real*8 :: beginTime, endTime, totalTime, temp, INNorm, DNorm, dlange

  read *, size

  allocate(A(size, size))
  allocate(Q(size, size))
  allocate(R(size, size))
  allocate(IN(size, size))
  allocate(D(size, size))
  allocate(QR(size, size))
  allocate(TAU(size))

  Q = 0
  R = 0

  lda = size
  tau = 2d0
  call randMatrix(IN, size)
  A(1:size,1:size) = IN(1:size,1:size)


  lwork = -1;
  call dgeqrf(size, size, A, lda, TAU, temp, lwork, info)

  lwork = int(temp)
  allocate(WORK(lwork))

  call CPU_TIME(beginTime)
  !QR
  call dgeqrf(size, size, A, lda, TAU, WORK, lwork, info)

  !R
  !call dormqr???
  do i = 1, size
    R(1:i, i) = A(1:i, i)
  enddo

  !Q
  call dorgqr(size, size, size, A, lda, TAU, WORK, LWORK, info)
  call CPU_TIME(endTime)

  Q = A
  totalTime = endTime - beginTime
  QR = matmul(Q, R)

  D = IN - QR

  INNorm = dlange('F', size, size, IN, size)
  DNorm = dlange('F', size, size, D, size)
  
  print *, DNorm / INNorm
  print *, totalTime

  deallocate(A)
  deallocate(WORK)
  deallocate(TAU)
  deallocate(D)
  deallocate(Q)
  deallocate(R)
  deallocate(QR)
  deallocate(IN)

end program QR_LAPACK


subroutine randMatrix(A, size)
  implicit none
  integer size
  real*8 A(size, size)
  call random_number(A)
  A = -1d0 + 2d0 * A
end subroutine randMatrix